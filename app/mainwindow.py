from PySide2 import QtCore, QtGui, QtWidgets


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

    def fullscreen(self):
        self.setWindowState(self.windowState() ^ QtGui.Qt.WindowFullScreen)

    def exit(self):
        self.close()
