import sys

from PySide2 import QtWidgets
try:
    from qt_material import apply_stylesheet
except ImportError:
    try:
        from pyside_material import apply_stylesheet
    except ImportError:
        def apply_stylesheet(app, theme):
            print('Could not apply stylesheet')

from app.mainwindow import MainWindow


def fmain():
    application = QtWidgets.QApplication(sys.argv)
    apply_stylesheet(application, theme='dark_teal.xml')

    window = MainWindow()
    window.show()
    sys.exit(application.exec_())
